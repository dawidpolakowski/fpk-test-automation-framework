# FPK test automation framework

Frompurplecloud.com test automation framework

## Getting started

This project is just a placeholder for Cypress test suite created for Frompurplecloud.com WordPress eCommerce website.

## Test cases

| no | id | title |
| ---      | ---      | ---      |
| 1 | FPK.01 |  Open landing page and check all sections |
| 2 | FPK.02 |  Check navigation | 
| 3 | FPK.03 |  Add item to basket |
| 4 | FPK.04 |  Buy item using test voucher |
| 5 | FPK.05 |  Send contact form |
